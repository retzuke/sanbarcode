<?PHP
            require('Animal.php');
            require('Ape.php');
            require('Frog.php');


            $sheep = new Animal("shaun");

            echo "Nama Animal :  $sheep->name <br>"; 
            echo "Legs: $sheep->legs <br>"; 
            echo "Cold blooded:$sheep->cold_blooded <br>";

            $kodok = new Frog("buduk");

            echo "Nama Animal :  $kodok->name <br>"; 
            echo "Legs: $kodok->legs <br>"; 
            echo "Cold blooded:$kodok->cold_blooded <br>";
            $kodok->jump();
            echo "<br>";
            
            $sungkong = new Ape ("kera sakti");
            echo "Nama Animal :  $sungkong->name <br>"; 
            echo "Legs: $sungkong->legs <br>"; 
            echo "Cold blooded:$sungkong->cold_blooded <br>";
            $sungkong->yell();

?>